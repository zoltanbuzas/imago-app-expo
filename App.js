import React from "react";
import { DefaultTheme, Provider as PaperProvider } from "react-native-paper";
import AppNavigator from "./src/navigation/AppNavigator";
import { PlayerStoreContainer } from "./src/unstated/PlayerStore";
// import { Provider as StoreProvider } from "react-redux";
// import store from "./src/redux/store";

const theme = {
  ...DefaultTheme,
  roundness: 2,
  colors: {
    ...DefaultTheme.colors,
    primary: "#3498db",
    accent: "#f1c40f",
  },
};

export default function App() {
  return (
    // <StoreProvider store={store}>
    <PlayerStoreContainer.Provider>
      <PaperProvider theme={theme}>
        <AppNavigator />
      </PaperProvider>
    </PlayerStoreContainer.Provider>
    // </StoreProvider>
  );
}
