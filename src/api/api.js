import uuid from "uuid";
import moment from "moment";
import Constants from "expo-constants";

const { manifest } = Constants;
// const api = manifest.packagerOpts.dev
//   ? manifest.debuggerHost.split(`:`).shift().concat(`:3000`)
//   : `imagoapi.villamkod.hu`;
const api = "imagoapi.villamkod.hu";

const url = `https://${api}/`;

export function getDailyPray(date) {
  let link = url + `dailypray/?format=json&pray_date=${date}`;
  console.log(link);
  return fetch(link).then((response) => response.json());
  // .then((events) => events.map((e) => ({ ...e, date: new Date(e.date) })))
}

export function getMonthlyPray(date) {
  let link = url + `monthlypray/day/${date}/`;
  return fetch(link).then((response) => response.json());
}

export function getExamenType(date) {
  let link = url + `examentype/?format=json`;
  return fetch(link)
    .then((response) => response.json())
    .then((json) => {
      return json.results;
    })
    .catch((error) => {
      console.error(error);
    });
}
