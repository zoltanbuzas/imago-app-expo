import React from "react";
import { StyleSheet, Text, View } from "react-native";

const HeaderBar = (props) => {
  return (
    <View style={props.subtitle ? styles.subtitleBar : styles.headerbar}>
      <View
        style={{
          flex: 1,
          flexDirection: "row",
          justifyContent: "center",
          alignContent: "center",
          alignItems: "center",
        }}
      >
        <Text
          style={{
            fontFamily: "Baloo2-Bold",
            fontSize: 30,
            color: "white",
          }}
        >
          {props.title}
        </Text>
        {props.subtext ? <Text style={{ fontSize: 16, color: "white" }}>&nbsp;| {props.subtext}</Text> : null}
      </View>
      {props.subtitle ? (
        <Text style={{ fontSize: 14, color: "white", textAlign: "center", paddingBottom: 10 }}>{props.subtitle}</Text>
      ) : null}
    </View>
  );
};

export default HeaderBar;

const styles = StyleSheet.create({
  headerbar: {
    backgroundColor: "black",
    height: 50,
  },
  subtitleBar: {
    backgroundColor: "black",
    height: 80,
  },
});
