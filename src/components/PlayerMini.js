import React from "react";
import {
  Dimensions,
  ImageBackground,
  StyleSheet,
  View,
  Text,
  Button,
  Slider,
  Image,
  TouchableHighlight,
  TouchableOpacity,
  TouchableNativeFeedback,
} from "react-native";
// import Player from "../components/Player";
// import { useSelector, useDispatch } from "react-redux";
// import { play_pause, stop } from "../redux/PlayerReducer";
import { LinearGradient } from "expo-linear-gradient";
import { ProgressBar, Colors } from "react-native-paper";

import { PlayerStoreContainer } from "../unstated/PlayerStore";
import { useNavigation } from "@react-navigation/native";

import Ionicons from "react-native-vector-icons/Ionicons";
import MaterialIcons from "react-native-vector-icons/MaterialIcons";
import FontAwesome5 from "react-native-vector-icons/FontAwesome5";
import AntDesign from "react-native-vector-icons/AntDesign";
import MaterialCommunityIcons from "react-native-vector-icons/MaterialCommunityIcons";

const BACKGROUND_COLOR = "gray";
const MAX_LIMIT = 30;

export default function PlayerMini() {
  const playerStore = PlayerStoreContainer.useContainer();
  const navigation = useNavigation();

  return (
    <View style={styles.miniPlayer}>
      <ProgressBar style={{}} progress={playerStore._getSeekSliderPosition()} color={Colors.orange600} />
      <View style={styles.controll}>
        <TouchableNativeFeedback
          underlayColor={BACKGROUND_COLOR}
          style={styles.controlText}
          onPress={() => {
            navigation.navigate("PlayerModalScreen");
          }}
        >
          <Text style={styles.text}>
            {playerStore.playbackInstanceName.length > MAX_LIMIT
              ? playerStore.playbackInstanceName.substring(0, MAX_LIMIT - 3) + "..."
              : playerStore.playbackInstanceName}
          </Text>
        </TouchableNativeFeedback>
        <TouchableOpacity
          underlayColor={BACKGROUND_COLOR}
          style={styles.controlBtn}
          onPress={playerStore.handlePlayPause}
        >
          {playerStore.isPlaying ? (
            <MaterialCommunityIcons name="pause" color="white" size={36} />
          ) : (
            <MaterialCommunityIcons name="play" color="white" size={36} />
          )}
        </TouchableOpacity>
      </View>
    </View>
  );
}

const { width } = Dimensions.get("window");
const styles = StyleSheet.create({
  miniPlayer: {
    // flex: 1,
    // height: 70,
    // flexDirection: "row",
    // alignItems: "stretch",
    // alignContent: "stretch",
    // justifyContent: "stretch",
    // flexGrow: 1,
    backgroundColor: "#000",
  },
  controll: {
    width: width,
    flexDirection: "row",
    alignItems: "center",
    height: 70,
  },
  controlBtn: {
    paddingHorizontal: 5,
    width: 70,
    alignItems: "center",
    justifyContent: "center",
    height: 70,
  },
  controlText: {
    flexGrow: 1,
  },
  text: {
    paddingLeft: 10,
    color: "white",
    fontSize: 20,
    lineHeight: 70,
    flexGrow: 1,
  },
});
