import React from "react";

import "react-native-gesture-handler";
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
import { createMaterialBottomTabNavigator } from "@react-navigation/material-bottom-tabs";

import DailyPrayScreen from "../screens/DailyPray";
import MounthlyPrayScreen from "../screens/MounthlyPray";
import ExamenScreen from "../screens/Examen";
import PlayerModalScreen from "../screens/PlayerModal";

import Ionicons from "react-native-vector-icons/Ionicons";
import MaterialIcons from "react-native-vector-icons/MaterialIcons";
import MaterialCommunityIcons from "react-native-vector-icons/MaterialCommunityIcons";
import FontAwesome5 from "react-native-vector-icons/FontAwesome5";

const Tab = createMaterialBottomTabNavigator();

const TabScreen = () => {
  return (
    <Tab.Navigator
      initialRouteName="DailyPray"
      activeColor="orange"
      inactiveColor="white"
      barStyle={{ backgroundColor: "#111", paddingTop: 4, paddingBottom: 2 }}
    >
      <Tab.Screen
        name="Examen"
        component={ExamenScreen}
        options={{
          tabBarLabel: "Examen",
          tabBarIcon: ({ color }) => (
            <MaterialCommunityIcons
              name="fire"
              color={color}
              size={30}
              style={{ marginBottom: 5, marginTop: -5, marginLeft: -2 }}
            />
          ),
        }}
      />
      <Tab.Screen
        name="DailyPray"
        component={DailyPrayScreen}
        options={{
          tabBarLabel: "DailyPrayScreen",
          tabBarIcon: ({ color }) => (
            <MaterialCommunityIcons
              name="bag-personal"
              color={color}
              size={28}
              style={{ marginBottom: 5, marginTop: -7 }}
            />
          ),
        }}
      />
      <Tab.Screen
        name="MonthlyPray"
        component={MounthlyPrayScreen}
        options={{
          tabBarLabel: "MonthlyPray",
          tabBarIcon: ({ color }) => (
            <FontAwesome5
              name="praying-hands"
              color={color}
              size={22}
              style={{ marginBottom: 2, marginTop: -4, marginLeft: -10, marginRight: -10 }}
            />
          ),
        }}
      />
    </Tab.Navigator>
  );
};

const RootStack = createStackNavigator();
const RootStackScreen = () => {
  return (
    <RootStack.Navigator headerMode="none" screenOptions={{ animationEnabled: false }} mode="Modal">
      <RootStack.Screen name="TabScreen" component={TabScreen} />
      <RootStack.Screen name="PlayerModalScreen" component={PlayerModalScreen} options={{ animationEnabled: true }} />
    </RootStack.Navigator>
  );
};

// export default createAppContainer(RootStack);
export default function AppNavigator() {
  return (
    <NavigationContainer>
      <RootStackScreen />
    </NavigationContainer>
  );
}
