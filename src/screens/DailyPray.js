import React, { useState, useEffect } from "react";
import { Text, View, StyleSheet, ImageBackground, ScrollView, SafeAreaView, StatusBar, Button } from "react-native";
import { LinearGradient } from "expo-linear-gradient";
import { useFonts } from "@use-expo/font";
import Constants from "expo-constants";
import { AppLoading } from "expo";
import CalendarStrip from "react-native-calendar-strip";
import moment from "moment";
import "moment/locale/hu"; // language must match config

// import MaterialCommunityIcons from "react-native-vector-icons/MaterialCommunityIcons";
// import { MaterialCommunityIcons } from "@expo/vector-icons";
import HeaderBar from "../components/HeaderBar";
import { getDailyPray } from "../api/Api";
import { PlayerStoreContainer } from "../unstated/PlayerStore";
import PlayerMini from "../components/PlayerMini";

// export const loadIcons = Promise.all([MaterialCommunityIcons.getImageSource("fire", 24)])
//   .then((sources) => {
//     let icons = {};
//     [icons.fire] = sources;
//     return icons;
//   })
// //   .catch((error) => error);
// export class IconExample extends React.Component {
//   console.log(render());
//   render() {
//     return <MaterialCommunityIcons name="fire" size={32} color="green" />;
//   }
// }

export default function DailyPrayScreen({ navigation }) {
  // const image = { uri: "https://reactjs.org/logo-og.png" };
  const image = require("../../assets/trees.png");
  const arrow_left = require("../../assets/left-arrow-white.png");
  const arrow_right = require("../../assets/right-arrow-white.png");
  let [fontsLoaded] = useFonts({
    "Baloo2-Bold": require("../../assets/fonts/Baloo2-Bold.ttf"),
  });

  const playerStore = PlayerStoreContainer.useContainer();

  let startDateNow = moment(); // today

  function datesBlacklistFunc(date) {
    return date.isoWeekday() === 6; // disable Saturdays
  }

  function onDateSelected(date) {
    setFormattedDate(date.format("YYYY. MMMM DD."));
    getDailyPray(date.format("YYYY-MM-DD"))
      .then((response) => {
        console.log("JSON DAILY:", response.results[0].title);
        let item = response.results[0];
        setContent(item);
        if (item.url !== null) {
          console.log(item.url);
          // playerStore.clearPlaylist();
          // playerStore.clearPlaylist();
          // playerStore.addPlaylistItem(item.title, item.url, false);
          playerStore.newPlaylist(item.title, item.url, false);
        }
      })
      .catch((error) => console.error("Error:", error));
  }
  // // Create a week's worth of custom date styles and marked dates.
  let customDatesStyles = [];
  // let markedDates = [];
  // for (let i=0; i<7; i++) {
  //     let date = startDate.clone().add(i, 'days');

  customDatesStyles.push({
    startDate: startDateNow, // Single date since no endDate provided
    dateNameStyle: { color: "white" },
    dateNumberStyle: { color: "white" },
    dateContainerStyle: {
      backgroundColor: "transparent",
      // border: "1px solid white",
      borderColor: "white",
      borderWidth: 1,
      borderRadius: 10,
      //   backgroundColor: `#${`#00000${((Math.random() * (1 << 24)) | 0).toString(16)}`.slice(-6)}`,
    },
  });
  //   onDateSelected(startDateNow.format("YYYY-MM-DD"));
  //     let dots = [{
  //     key: i,
  //     color: 'red',
  //     selectedDotColor: 'yellow',
  //     }];
  //     if (i % 2) {  // add second dot to alternating days
  //     dots.push({
  //         key: i + 'b',
  //         color: 'blue',
  //         selectedDotColor: 'yellow',
  //     });
  //     }
  //     markedDates.push({
  //     date,
  //     dots,
  //     });
  // }

  const [selectedDate, setSelectedDate] = useState(startDateNow);
  // const [icons, setIcons] = useState({});
  const [content, setContent] = useState({
    date: startDateNow.format("YYYY-MM-DD"),
    title: "T",
    content: "C",
  });
  // const [customDatesStylesList, setCustomDatesStyles] = useState(customDatesStyles);
  // const [markedDatesList, setMarkedDates] = useState(markedDates);
  const [startDate, setStartDate] = useState(startDateNow);
  const [formattedDate, setFormattedDate] = useState(startDateNow.format("YYYY-MMMM-DD"));
  let count = 1;
  useEffect(() => {
    onDateSelected(startDateNow);
  }, [count]);

  if (!fontsLoaded) {
    return <AppLoading />;
  } else {
    return (
      <SafeAreaView style={styles.containertext}>
        <StatusBar barStyle="light-content" backgroundColor="#000000" />
        <ScrollView style={styles.body}>
          <View>
            <HeaderBar title="Imago" subtext="Examen az élethez" />
            <View>
              <ImageBackground source={image} style={styles.image}>
                <CalendarStrip
                  scrollable
                  calendarAnimation={{ type: "sequence", duration: 30 }}
                  daySelectionAnimation={{
                    // type: "background",
                    type: "border",
                    duration: 300,
                    highlightColor: "#555",
                    borderWidth: 1,
                    borderHighlightColor: "white",
                  }}
                  style={{
                    height: 160,
                    paddingTop: 40,
                    paddingBottom: 0,
                    backgroundColor: "rgba(0, 0, 0, 0.25)",
                    borderRadius: 40,
                    padding: 3,
                  }}
                  calendarHeaderStyle={{
                    color: "white",
                    fontSize: 20,
                    textShadowColor: "rgba(0, 0, 0, 1)",
                    textShadowOffset: { width: 0, height: 0 },
                    textShadowRadius: 30,
                    // backgroundColor: "rgba(0, 0, 0, 0.25)",
                    // borderRadius: 40,
                    paddingBottom: 30,
                  }}
                  // calendarHeaderFormat={"YYYY MMMM"}
                  calendarColor={"transparent"}
                  dateContainerStyle={{ marginTop: 40 }}
                  dateNumberStyle={{ color: "white", fontSize: 18 }}
                  highlightDateNumberStyle={{ color: "white", fontSize: 18 }}
                  dateNameStyle={{ color: "white", fontSize: 10 }}
                  highlightDateNameStyle={{ color: "white", fontSize: 10 }}
                  iconContainer={{ flex: 0.1, alignItems: "stretch" }}
                  customDatesStyles={customDatesStyles}
                  iconLeft={arrow_left}
                  iconRight={arrow_right}
                  //   markedDates={markedDatesList}
                  // datesBlacklist={datesBlacklistFunc}
                  onDateSelected={onDateSelected}
                  useIsoWeekday={false}
                />
              </ImageBackground>
              <View style={styles.body}>
                <LinearGradient
                  colors={["rgba(0,0,0,0.8)", "transparent"]}
                  style={{
                    position: "absolute",
                    left: 0,
                    right: 0,
                    top: 0,
                    height: "90%",
                  }}
                />
                <View style={styles.textcontainer}>
                  <Text style={styles.texttitle}>{formattedDate}</Text>
                  <Text style={styles.texttitle}>{content.title}</Text>
                  <Text style={styles.textsmall}></Text>
                  <Text style={styles.text}>Evangélium {content.content}</Text>
                  <Text style={styles.text}></Text>

                  {/* <Button title="Play" onPress={() => navigation.navigate("PlayerModalScreen")} color="#000" /> */}
                </View>
              </View>
            </View>
          </View>
        </ScrollView>
        <PlayerMini />
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
    justifyContent: "center",
  },
  containertext: {
    flex: 1,
    // marginTop: Constants.statusBarHeight,
    alignItems: "stretch",
  },
  tabbar: {
    backgroundColor: "black",
  },
  body: {
    flex: 1,
    backgroundColor: "#733508",
  },
  textcontainer: {
    paddingHorizontal: 10,
    paddingVertical: 10,
  },
  texttitle: {
    fontWeight: "bold",
    fontSize: 20,
    color: "white",
  },
  text: {
    fontSize: 20,
    color: "white",
    textAlign: "justify",
    lineHeight: 28,
  },
  textsmall: {
    fontSize: 10,
    color: "white",
  },
  image: {},
});
