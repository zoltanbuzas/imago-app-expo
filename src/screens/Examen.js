import React, { useState, useEffect } from "react";
import {
  Text,
  View,
  StyleSheet,
  ScrollView,
  SafeAreaView,
  StatusBar,
  FlatList,
  TouchableOpacity,
  TouchableHighlight,
} from "react-native";
// import Player from "../components/Player";
import { Card, Title, Surface, TouchableRipple } from "react-native-paper";
import { LinearGradient } from "expo-linear-gradient";
// import Constants from "expo-constants";
import HeaderBar from "../components/HeaderBar";

import MaterialIcons from "react-native-vector-icons/MaterialIcons";
import MaterialCommunityIcons from "react-native-vector-icons/MaterialCommunityIcons";
import FontAwesome5 from "react-native-vector-icons/FontAwesome5";

import { getExamenType } from "../api/Api";

export function ExamenTypeList({ examenType }) {
  return (
    <>
      <HeaderBar title="Examen" subtitle="Minden pillant arra van, hogy Istennel éljem az élet játékát" />
      <View style={styles.wrapper}>
        <LinearGradient
          colors={[gradientBg, "transparent"]}
          style={{
            position: "absolute",
            left: 0,
            right: 0,
            top: 0,
            height: "90%",
          }}
        />

        <FlatList
          style={styles.flatList}
          data={examenType}
          numColumns={2}
          // keyExtractor={(item, index) => item.id}
          renderItem={(item) => (
            <Surface style={styles.surface}>
              <TouchableOpacity
                underlayColor={"rgba(255, 255, 255, 0.32)"}
                style={styles.touchable}
                onPress={() => console.log("Pressed")}
                // disabled={this.state.isLoading}
              >
                <View styles={styles.btnContent}>
                  <View style={{ justiftyContent: "center", alignItems: "center" }}>
                    <MaterialCommunityIcons
                      name={examenType[item.index].icon_name}
                      color={iconColor}
                      size={iconFontSize}
                    />
                  </View>

                  <Text style={(styles.text, styles.btnContentItem)}>{examenType[item.index].name}</Text>
                  <Text style={(styles.text, styles.btnContentItem)}>{examenType[item.index].info}</Text>
                </View>
              </TouchableOpacity>
            </Surface>
          )}
        />
      </View>
    </>
  );
}

export function Examen() {
  return (
    <View style={styles.wrapper}>
      <LinearGradient
        colors={[gradientBg, "transparent"]}
        style={{
          position: "absolute",
          left: 0,
          right: 0,
          top: 0,
          height: "90%",
        }}
      />
      <Text>Examen</Text>
    </View>
  );
}

export default function ExamenScreen() {
  const [isLoading, setIsLoading] = useState(true);
  const [isExamen, setIsExamen] = useState(false);
  const [examenType, setExamenType] = useState([]);
  const [selectedExamen, setSelectedExamen] = useState({});
  useEffect(() => {
    getExamenType().then((response) => {
      // console.log("JSON ExamenType:", response);
      setExamenType(response);
      setIsLoading(false);
    });
    console.log("Effect");
  }, []);
  return (
    <SafeAreaView style={styles.container}>
      <StatusBar barStyle="light-content" backgroundColor="#000000" />
      {isLoading ? (
        <View>
          <Text>Loading</Text>
        </View>
      ) : isExamen ? (
        <Examen />
      ) : (
        <ExamenTypeList examenType={examenType} />
      )}
    </SafeAreaView>
  );
}

const iconColor = "orange";
const iconFontSize = 40;
const gradientBg = "rgba(255, 190, 0,0.8)";

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fb7747",
    // backgroundColor: "#FF4F00",
    // backgroundColor: "#733508",
    // marginTop: Constants.statusBarHeight,
  },
  wrapper: {
    // backgroundColor: "#733508",
    flexGrow: 1,
    // flex: 1,
    marginBottom: 20,
  },
  flatList: {
    paddingTop: 15,
    marginBottom: 70,
    // backgroundColor: "#733508",
    // flexGrow: 1,
    // paddingBottom: 20,
  },
  text: {
    fontSize: 16,
    color: "white",
    textAlign: "center",
  },
  surface: {
    // flex: 1,
    // flexGrow: 1,
    marginLeft: "6%",
    marginRight: "0%",
    marginTop: 15,
    marginBottom: 15,
    // padding: 8,
    // height: 80,
    // width: 80,
    width: "41%",
    // alignItems: "center",
    // justifyContent: "center",
    // alignContent: "center",
    elevation: 8,
    backgroundColor: "black",
    borderRadius: 5,
  },
  touchable: {
    // flex: 1,
    // flexGrow: 1,
    // height: 300,
    // alignItems: "stretch",
    //   flex: 1,
    //   flexDirection: "column",
    //   width: "100%",
    // alignItems: "center",
    // justifyContent: "center",
    // alignContent: "center",
    // textAlign: "center",
    // backgroundColor: "red",
    padding: 8,
  },
  btnContent: {
    // flexGrow: 1,
    // flex: 1,
    // flexDirection: "column",
    // width: "100%",
    // alignItems: "stretch",
    // justifyContent: "center",
    // alignContent: "stretch",
    // textAlign: "center",
    // backgroundColor: "red",
    padding: 8,
  },
  btnContentItem: {
    // flexGrow: 1,
    //   flex: 1,
    //   flexDirection: "column",
    // width: "100%",
    // alignItems: "center",
    // justifyContent: "center",
    // alignContent: "center",
    fontSize: 16,
    color: "white",
    textAlign: "center",
    // textAlign: "center",
    // backgroundColor: "red",
    // padding: 8,
  },
});
