import React from "react";
import { Text, View, StyleSheet } from "react-native";
import { LinearGradient } from "expo-linear-gradient";

export default function MonthlyPrayScreen() {
  return (
    <View style={styles.container}>
      <LinearGradient
        colors={["rgba(0,0,0,0.8)", "transparent"]}
        style={{
          position: "absolute",
          left: 0,
          right: 0,
          top: 0,
          height: "90%",
        }}
      />
      <Text style={styles.text}>MonthlyPrayScreen!</Text>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#333",
    alignItems: "center",
    justifyContent: "center",
  },
  tabbar: {
    backgroundColor: "black",
  },
  headerbar: {
    backgroundColor: "#000000",
    color: "white",
    height: 50,
  },
  text: {
    color: "white",
  },
});
