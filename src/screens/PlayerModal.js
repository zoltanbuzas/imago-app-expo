import React from "react";
import {
  Dimensions,
  ImageBackground,
  StyleSheet,
  View,
  Text,
  Button,
  Slider,
  Image,
  TouchableHighlight,
  TouchableOpacity,
} from "react-native";
// import Player from "../components/Player";
// import { useSelector, useDispatch } from "react-redux";
// import { play_pause, stop } from "../redux/PlayerReducer";
import { LinearGradient } from "expo-linear-gradient";
import { PlayerStoreContainer } from "../unstated/PlayerStore";

import Ionicons from "react-native-vector-icons/Ionicons";
import MaterialIcons from "react-native-vector-icons/MaterialIcons";
import FontAwesome5 from "react-native-vector-icons/FontAwesome5";
import AntDesign from "react-native-vector-icons/AntDesign";
import MaterialCommunityIcons from "react-native-vector-icons/MaterialCommunityIcons";

const BACKGROUND_COLOR = "gray";
const image = require("../../assets/music_background.png");

export default function PlayerModal({ navigation }) {
  const playerStore = PlayerStoreContainer.useContainer();

  return (
    <View style={styles.container}>
      <ImageBackground source={image} style={styles.imageBackground}>
        <LinearGradient
          colors={["rgba(0,0,0,0.8)", "transparent"]}
          style={{
            position: "absolute",
            left: 0,
            right: 0,
            top: 0,
            height: "90%",
          }}
        />
        <View style={styles.topbar}>
          <TouchableOpacity
            underlayColor={BACKGROUND_COLOR}
            style={styles.button}
            onPress={() => navigation.pop()}
            // disabled={this.state.isLoading}
          >
            <Ionicons name="ios-arrow-down" color="white" size={30} />
          </TouchableOpacity>
        </View>
        <View style={styles.imageWrapper}>
          <Image style={styles.image} source={require("../../assets/music_default.png")} />
          <View>
            <Slider
              style={styles.playbackSlider}
              minimumTrackTintColor="#ffffff"
              maximumTractTintColor="#123123"
              value={playerStore._getSeekSliderPosition()}
              onValueChange={playerStore._onSeekSliderValueChange}
              onSlidingComplete={playerStore._onSeekSliderSlidingComplete}
              disabled={playerStore.isLoading}
              thumbTintColor="#1EB1FC"
            />
          </View>
          <View style={styles.controll}>
            <TouchableHighlight underlayColor={BACKGROUND_COLOR} style={styles.controlBtnSmall}>
              <Text></Text>
            </TouchableHighlight>
            <View style={styles.controlSpacer}></View>
            <TouchableOpacity
              underlayColor={BACKGROUND_COLOR}
              style={styles.controlBtn}
              onPress={playerStore.handlePlayPause}
            >
              {playerStore.isPlaying ? (
                <MaterialCommunityIcons name="pause-circle" color="white" size={90} />
              ) : (
                <MaterialCommunityIcons name="play-circle" color="white" size={90} />
              )}
            </TouchableOpacity>
            <View style={styles.controlSpacer}></View>
            <TouchableOpacity
              underlayColor={BACKGROUND_COLOR}
              style={styles.controlBtnSmall}
              onPress={playerStore.handleStop}
            >
              <MaterialCommunityIcons name="stop" color="white" size={40} />
            </TouchableOpacity>
          </View>
        </View>
      </ImageBackground>
    </View>
  );
}

const { width } = Dimensions.get("window");
const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: "row",
    alignItems: "stretch",
    alignContent: "stretch",
    // justifyContent: "stretch",
    flexGrow: 1,
    backgroundColor: "#222",
  },
  topbar: {
    flexDirection: "row",
    alignSelf: "stretch",
    justifyContent: "space-between",
    alignItems: "center",
  },
  wrapper: {},
  button: {
    padding: 5,
    marginHorizontal: 20,
    marginVertical: 10,
    borderRadius: 50,
  },
  imageWrapper: {
    // padding: "20%",
    alignItems: "center",
  },
  imageBackground: { width: width },
  image: {
    resizeMode: "cover",
    width: width - 80,
    height: width - 80,
    borderRadius: 30,
  },
  controll: {
    marginTop: 20,
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
  },
  controlBtn: {
    marginHorizontal: 5,
  },
  controlSpacer: {
    marginHorizontal: 5,
    flexGrow: 1,
    backgroundColor: "red",
  },
  controlBtnSmall: {
    marginHorizontal: 5,
    width: "20%",
    alignItems: "center",
  },
  playbackSlider: {
    marginTop: 20,
    alignItems: "center",
    width: width - 60,
    // height: 10,
    // width: "20%",
    // alignItems: "center",
  },
});
