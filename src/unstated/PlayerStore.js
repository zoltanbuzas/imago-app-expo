import { useState, useEffect } from "react";
// import shortid from "shortid";
import { createContainer } from "unstated-next";
import { Audio } from "expo-av";

class PlaylistItem {
  constructor(name, uri, isVideo) {
    this.name = name;
    this.uri = uri;
    this.isVideo = isVideo;
  }
}

const LOOPING_TYPE_ALL = 0;
const LOOPING_TYPE_ONE = 1;

// const { width: DEVICE_WIDTH, height: DEVICE_HEIGHT } = Dimensions.get("window");
// const BACKGROUND_COLOR = "#FFF8ED";
// const DISABLED_OPACITY = 0.5;
const FONT_SIZE = 14;
const LOADING_STRING = "... loading ...";
const BUFFERING_STRING = "...buffering...";
const RATE_SCALE = 3.0;
// const VIDEO_CONTAINER_HEIGHT = (DEVICE_HEIGHT * 2.0) / 5.0 - FONT_SIZE * 2;

export const useStore = () => {
  const [playlist, setPlaylist] = useState([]);
  const [playbackInstance, setPlaybackInstance] = useState(null);
  const [isPlaying, setIsPlaying] = useState(false);
  const [index, setIndex] = useState(0);
  const [isSeeking, setIsSeeking] = useState(false);
  const [shouldPlayAtEndOfSeek, setShouldPlayAtEndOfSeek] = useState(false);
  const [showVideo, setShowVideo] = useState(false);
  const [playbackInstanceName, setPlaybackInstanceName] = useState(LOADING_STRING);
  const [loopingType, setLoopingType] = useState(LOOPING_TYPE_ALL);
  const [muted, setMuted] = useState(false);
  const [playbackInstancePosition, setPlaybackInstancePosition] = useState(null);
  const [playbackInstanceDuration, setPlaybackInstanceDuration] = useState(null);
  const [shouldPlay, setShouldPlay] = useState(false);
  const [isBuffering, setIsBuffering] = useState(false);
  const [isLoading, setIsLoading] = useState(true);
  //  const [fontLoaded,setFontLoaded] = useState(false);
  const [shouldCorrectPitch, setShouldCorrectPitch] = useState(true);
  const [volume, setVolume] = useState(1.0);
  const [rate, setRate] = useState(1.0);
  //  const [videoWidth,setVideoWidth] = useState(DEVICE_WIDTH);
  //  const [videoHeight,setVideoHeight] = useState(VIDEO_CONTAINER_HEIGHT);
  const [poster, setPoster] = useState(false);
  const [useNativeControls, setUseNativeControls] = useState(false);
  const [fullscreen, setFullscreen] = useState(false);
  const [throughEarpiece, setThroughEarpiece] = useState(false);
  const source = {
    uri: "https://s3.amazonaws.com/exp-us-standard/audio/playlist-example/Comfort_Fit_-_03_-_Sorry.mp3",
  };
  const _loadNewPlaybackInstance = async (playing) => {
    if (playbackInstance != null) {
      await playbackInstance.unloadAsync();
      playbackInstance.setOnPlaybackStatusUpdate(null);
      setPlaybackInstance(null);
    }

    const source = { uri: playlist[index].uri };
    const initialStatus = {
      shouldPlay: playing,
      rate: rate,
      shouldCorrectPitch: shouldCorrectPitch,
      volume: volume,
      isMuted: muted,
      isLooping: loopingType === LOOPING_TYPE_ONE,
      // // UNCOMMENT THIS TO TEST THE OLD androidImplementation:
      // androidImplementation: 'MediaPlayer',
    };

    const { sound, status } = await Audio.Sound.createAsync(source, initialStatus, _onPlaybackStatusUpdate);
    setPlaybackInstance(sound);

    _updateScreenForLoading(false);
  };
  const _updateScreenForLoading = (isLoading) => {
    if (isLoading) {
      setShowVideo(false);
      setIsPlaying(false);
      setPlaybackInstanceName(LOADING_STRING);
      setPlaybackInstanceDuration(null);
      setPlaybackInstancePosition(null);
      setIsLoading(true);
    } else {
      setPlaybackInstanceName(playlist[index].name);
      setShowVideo(playlist[index].isVideo);
      setIsLoading(false);
    }
  };
  const _onPlaybackStatusUpdate = (status) => {
    console.log("playlist StatusUpdate Count:", playlist.length);
    console.log("status UPDATE:", status.isLoaded);
    if (status.isLoaded) {
      setPlaybackInstancePosition(status.positionMillis);
      setPlaybackInstanceDuration(status.durationMillis);
      setShouldPlay(status.shouldPlay);
      setIsPlaying(status.isPlaying);
      setIsBuffering(status.isBuffering);
      setRate(status.rate);
      setMuted(status.isMuted);
      setVolume(status.volume);
      setLoopingType(status.isLooping ? LOOPING_TYPE_ONE : LOOPING_TYPE_ALL);
      setShouldCorrectPitch(status.shouldCorrectPitch);

      if (status.didJustFinish && !status.isLooping) {
        _advanceIndex(true);
        _updatePlaybackInstanceForIndex(true);
      }
    } else {
      if (status.error) {
        console.log(`FATAL PLAYER ERROR: ${status.error}`);
      }
    }
  };

  const _advanceIndex = (forward) => {
    index = (index + (forward ? 1 : playlist.length - 1)) % playlist.length;
  };

  const _updatePlaybackInstanceForIndex = async (playing) => {
    _updateScreenForLoading(true);

    // setVideoWidth(DEVICE_WIDTH);
    // setVideoHeight(VIDEO_CONTAINER_HEIGHT);

    _loadNewPlaybackInstance(playing);
  };
  // SLIDER POSITION

  const _onSeekSliderValueChange = (value) => {
    if (playbackInstance != null && !isSeeking) {
      setIsSeeking(true);
      setShouldPlayAtEndOfSeek(shouldPlay);
      playbackInstance.pauseAsync();
    }
  };

  const _onSeekSliderSlidingComplete = async (value) => {
    if (playbackInstance != null) {
      setIsSeeking(false);
      const seekPosition = value * playbackInstanceDuration;
      if (shouldPlayAtEndOfSeek) {
        playbackInstance.playFromPositionAsync(seekPosition);
      } else {
        playbackInstance.setPositionAsync(seekPosition);
      }
    }
  };

  const _getSeekSliderPosition = () => {
    if (playbackInstance != null && playbackInstancePosition != null && playbackInstanceDuration != null) {
      return playbackInstancePosition / playbackInstanceDuration;
    }
    return 0;
  };

  // PLAYLIST MANAGE

  const newPlaylist = async (name, uri, isVideo) => {
    console.log("playlist NEWLIST START:", playlist.length);
    if (playbackInstance != null) {
      await playbackInstance.unloadAsync();
      // this.playbackInstance.setOnPlaybackStatusUpdate(null);
      setPlaybackInstance(null);
    }
    const item = new PlaylistItem(name, uri, isVideo);
    setPlaylist([item]);
    console.log("playlist NEWLIST END:", playlist.length);
  };

  const clearPlaylist = async (name, uri, isVideo) => {
    console.log("playlist clear START:", playlist.length);
    if (playbackInstance != null) {
      await playbackInstance.unloadAsync();
      // this.playbackInstance.setOnPlaybackStatusUpdate(null);
      setPlaybackInstance(null);
    }
    setPlaylist([]);
    console.log("playlist clear END:", playlist.length);
  };

  const addPlaylistItem = (name, uri, isVideo) => {
    console.log("playlist ADD START:", playlist.length);
    const item = new PlaylistItem(name, uri, isVideo);
    setPlaylist([...playlist, item]);
    console.log("playlist ADD END:", playlist.length);
  };
  useEffect(() => {
    console.log("playlist CHANGE Count:", playlist.length);
    if (playbackInstance == null && playlist.length == 1) {
      _loadNewPlaybackInstance(false);
    }
    console.log("playlist CHANGE END Count:", playlist.length);
  }, [playlist]);

  const removePlaylistItem = (index) => {
    if (index > -1 && index < playlist.length) {
      let newPlaylist = [...playlist];
      newPlaylist.splice(index, 1);
      setPlaylist(newPlaylist);
    }
  };

  //PLAYER STATE

  // PLAYER CONTORL
  const handlePlayPause = (event) => {
    if (playbackInstance != null) {
      if (isPlaying) {
        playbackInstance.pauseAsync();
      } else {
        playbackInstance.playAsync();
      }
    }
  };
  const handleStop = (event) => {
    if (playbackInstance != null) {
      playbackInstance.stopAsync();
    }
  };

  return {
    handleStop,
    handlePlayPause,
    isPlaying,
    isLoading,
    playlist,
    addPlaylistItem,
    removePlaylistItem,
    newPlaylist,
    clearPlaylist,
    _getSeekSliderPosition,
    _onSeekSliderValueChange,
    _onSeekSliderSlidingComplete,
    playbackInstanceName,
  };
};

export const PlayerStoreContainer = createContainer(useStore);
